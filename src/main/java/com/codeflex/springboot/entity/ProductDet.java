package com.codeflex.springboot.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "productdet")
public class ProductDet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long iddet;

    private String ket;

    public long getIddet() {
        return iddet;
    }

    public void setIddet(long iddet) {
        this.iddet = iddet;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    @ManyToOne
    @JoinColumn(name="idprod")
    @JsonIgnoreProperties("productdet")
    private Product product;


    public void setProduct(Product product) {
        this.product = product;
    }

}
