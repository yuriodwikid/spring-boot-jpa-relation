package com.codeflex.springboot.controller;

import com.codeflex.springboot.entity.Product;
import com.codeflex.springboot.entity.ProductDet;
import com.codeflex.springboot.repo.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class RestApiProductController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiProductController.class);

    @Autowired
    private ProductRepo productRepo;

    // -------------------Retrieve All Products--------------------------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<Product>> listAllProducts() throws SQLException, ClassNotFoundException {
        logger.info("[Finding all product in database]");
        List<Product> products = productRepo.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    // -------------------Retrieve Single Product by ID------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") int id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Product with id {}", id);
        Product product = productRepo.findById(id).get();

        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    // -------------------Create a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createProductNew(@RequestBody Map<String, Object> payload) throws SQLException, ClassNotFoundException {
        //logger.info("Creating Product : {}", product);
        //logger.info("Creating Product : {}", product.getName());

        Product product = new Product();
        product.setName(payload.get("name").toString());

        int i = 0;

        @SuppressWarnings("unchecked")
        List<Map<String, Object>> prodDet = (List<Map<String, Object>>) payload.get("productDet");

        for (Map<String, Object> prodDetObj : prodDet) {
            logger.info("Adding Detail : {}", prodDetObj.get("ket").toString());
            ProductDet productDet = new ProductDet();
            productDet.setKet(prodDetObj.get("ket").toString());
            product.setCategoryId(Integer.parseInt(payload.get("categoryId").toString()));
            product.setPrice(Double.parseDouble(payload.get("price").toString()));

            product.addProdDet(productDet);
        }

        productRepo.save(product);

       /*int idGet = ((int) productRepo.save(product).getId());

        logger.info("Creating Product idGet : {}", idGet);
        //logger.info("Creating Product Detail : {}", product.getProductDet());

        productRepo.save(product);*/

        return new ResponseEntity<>(product, HttpStatus.CREATED);
    }
    // ------------------- Update a Product ------------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") int id, @RequestBody Map<String, Object> payload) throws SQLException, ClassNotFoundException {
        //request body dlem bentuk hash map karna dia bentuknya ga sekedar objek 1, dia itu kompleks
        //kadang one to manynya itu ga cuma ke 1 tabel aja
        logger.info("Updating Product with id {}", id);

        Product currentProduct = productRepo.findById(id).get();

        currentProduct.setCategoryId(Integer.parseInt(payload.get("categoryId").toString()));
        currentProduct.setName(payload.get("name").toString());
        currentProduct.setPrice(Double.parseDouble(payload.get("price").toString()));

        int i = 0;

        List<ProductDet> productDetUpdate = currentProduct.getProductDet();
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> prodDet = (List<Map<String, Object>>) payload.get("productDet");
        for (Map<String, Object> prodDetObj : prodDet) {
            logger.info("Adding Detail : {}", prodDetObj.get("ket").toString());
            productDetUpdate.get(i).setKet(prodDetObj.get("ket").toString());
            i++;
        }

        currentProduct.setProductDet(productDetUpdate);
        productRepo.save(currentProduct);
        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }
    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Product with id {}", id);

        Product product = productRepo.findById(id).get();

        productRepo.deleteById(id);
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

    // ------------------- Delete All Products-----------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.DELETE)
    public ResponseEntity<Product> deleteAllProducts() throws SQLException, ClassNotFoundException {
        logger.info("Deleting All Products");

        productRepo.deleteAll();
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }


}